# The Dot Factory #

This is a clone of a project by Eran Duchan. Please refer to [his blog article ](http://www.eran.io/the-dot-factory-an-lcd-font-and-image-generator/)for background information and theory of operation. The original repository is available at https://github.com/pavius/the-dot-factory

This fork adds the ability to generate C# code, for use on Netduino and other .NET Micro Framework based devices.


### What is this repository for? ###

* Allows quick and easy generation of bitmapped fonts for dot matrix displays, using TrueType and OpenType fonts as the template.
* Specifically designed to work with the [SSD1306 OLED Display Driver ](https://bitbucket.org/tigra-astronomy/ta.netmf.devices.ssd1306)from [Tigra Astronomy ](http://tigra-astronomy.com/)(free, MIT license).
* Allows generation of only a subset of the characters in the font. For example, it is extremely simple to generate a font containing only numbers. Bitmapped fonts can take up a lot of code space so this can be a life-saver. Just paste in the text that you need to display and the program works out which characters to generate.
